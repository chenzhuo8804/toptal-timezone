var current_user;
var data = {};

var updateId = {};

function ValidateForm() {
  console.log("start");
  var username = document.getElementById("user_name").value;
  var password = document.getElementById("password").value;
  var email = document.getElementById("email").value;

  if (password == "") {
    alert('password is required.');
  }
  if (username == "") {
    alert('User name is required.');
  }
  if (email.indexOf("@") < 1 || email.indexOf(".") < 1) {
    alert('Please enter a valid email address.');
  }
  //alert(frm.email.value)
  createUser(username, password, email);
}


function createUser(userInfo, passwordInfo, emailInfo) {
  $.post('http://localhost:8000/api/auth/register/', {username: userInfo, password:passwordInfo, email:emailInfo}).done(function(data) {
    $.post("http://localhost:8000/api-token-auth/", {'username': userInfo, 'password':passwordInfo})
      .done(function(data) {
        Cookies.set('toptal-token', data['token']);
        console.log(Cookies.get('toptal-token'));
        location.href = 'index.html';
      }).fail(function() {
        alert('Username or password error');
      });
  }).fail(function(XMLHttpRequest, textStatus, errorThrown) {
    console.log(errorThrown);
  });
}

$("#login_link").bind("click", function() {
  var username = document.getElementById("username").value;
  var password = document.getElementById("password1").value;

  // Parse.User.logIn(username, password, {
  //     success: function(user) {
  //         var currentUser = Parse.User.current();
  //         alert(currentUser.get("username"));
  //         location.href = "index.html";
  //         // alert("log in success");
  //         // Do stuff after successful login.
  //     },
  //     error: function(user, error) {
  //         //$elem = $("#r");
  //         $("#login_error").text("Password or User not correct! Please try again!").css("color", "red");
  //         $('#login_error').css('line-height', '50px');
  //         $("#login_error").show();
  //         // The login failed. Check error to see why.
  //     }
  // });

  $.post("http://localhost:8000/api-token-auth/", {'username': username, 'password':password})
    .done(function(data) {
      Cookies.set('toptal-token', data['token']);
      console.log(Cookies.get('toptal-token'));
      location.href = 'index.html';
    }).fail(function(data) {
      alert('Username or password error');
    });
});

function postChange(id, newValue) {
  var token = Cookies.get('toptal-token');
  var name;

  if (newValue) {
    name = newValue;
  } else {
    var nameId = '#'+'Name'+id;
    name = $(nameId).text();
  }
  var timezone = $('#' + 'Timezone' + id).find(':selected').text();
  console.log(name);
  console.log(timezone);
  var url = 'http://localhost:8000/models/timezone/' + id + '/';

  $.ajax({
    url: url,
    type: 'PUT',
    headers: {Authorization: 'Token ' + token},
    data: {pk:id, name: name, timezone: timezone}
  });
}

function refreshTable(url) {
  // This data should come from request
  $.fn.editable.defaults.mode = 'inline';
  var token = Cookies.get('toptal-token');
  var tzNames = moment.tz.names();
  var optionsAsString = '';
  tzNames.forEach(function(tz) {
    var tzStr = moment().tz(tz).format('Z');
    optionsAsString += "<option value='" + tz + "'>" + tz + "</option>";
  });

  $('#record_table').empty();
  data = {};
  $.ajax({
    url: url,
    type: 'GET',
    dataType: 'json',
    headers: {Authorization:'Token '+token},
    success: function(resp) {
      console.log(resp);

      resp.forEach(function(item){
        var tds = '<tr>';
        var key = item['pk'];
        var nameId = 'Name' + key.toString();
        var timeId = 'Time' + key.toString();
        var timezoneId = 'Timezone' + key.toString();
        var buttonId = 'Button' + key.toString();

        data[key] = {name:item['name'], timezone:item['timezone']};

        // tds += '<td hidden class="id">' + item['id'] + '</td>';
        tds += '<td>' + '<a href="#" id="' + nameId + '" data-type="text" data-pk="1" data-title="Enter username">' + item['name'] +'</a></td>';
        tds += '<td id="' + timeId + '" width=250>' + "</td>";
        tds += '<td>' + '<select class="form-control" id="' + timezoneId + '" style="min-width:160px">' + optionsAsString + '</select></td>';
        tds += '<td><button type="button" class="btn btn-danger btn-sm delete_row" id="' + buttonId + '">Delete</button></td>';
        tds += '</tr>';

        if ($('#record_table', this).length > 0) {
          $('#record_table', this).append(tds);
        } else {
          $('#record_table').append(tds);
        }

        $("#" + timeId).html(moment().format());
        $("#" + timezoneId).val(data[key]['timezone']);
        $("#" + timezoneId).change(function() {
          var id = this.id.replace('Timezone', '');
          postChange(id, '');
        });
        updateId["#" + timeId] = "#" + timezoneId;
        $("#" + nameId).editable({
          emptytext: 'Name can not be empty',
          success: function(response, newValue) {
            if (newValue) {
              postChange(this.id.replace('Name', ''), newValue);
            }
          }
        });
      });
    }
  });
};


setInterval(function() {
  for (var key in updateId) {
    // console.log(key, updateId[key]);
    var tzSelected = $(updateId[key]).find(':selected').text();
    if (tzSelected) {
      // console.log(tzSelected);
      $(key).html(moment().tz(tzSelected).format('MMMM Do YYYY, h:mm:ss Z'));
    }
  }
}, 100);


function refreshTable_old() {
  var query = new Parse.Query('calorie_Data');
  query.equalTo("user", Parse.User.current());
  query.find({
    success: function(results) {
      var calorie_summary = 0;
      // Why is not working?
      $('#record_table').remove();
      // $('#record_table').bootstrapTable('removeAll');
      for (var i = 0; i < results.length; i++) {
        calorie_summary = calorie_summary + parseFloat(results[i].get('calorie'));
        var tds = '<tr>';
        var objId = results[i].objectId;
        var time = results[i].get('time');
        var options = {
          weekday: "long",
          year: "numeric",
          month: "short",
          day: "numeric",
          hour: "2-digit",
          minute: "2-digit"
        };
        tds += '<td class="Time">' + results[i].get('time').toLocaleDateString("en-US", options) + '</td>';
        tds += '<td class="meal">' + results[i].get('meal') + '</td>';
        tds += '<td class="calorie">' + results[i].get('calorie') + '</td>';
        tds += '<td><button type="button" class="btn btn-success btn-sm edit_record">Edit</button></td>';
        tds += '<td><button type="button" class="btn btn-danger btn-sm delete_row">Delete</button></td>';
        tds += '<td hidden class="id">' + objId + '</td>';
        tds += '<td hidden class="time_date">' + results[i].get('time') + '</td>';
        tds += '</tr>';

        if ($('#record_table', this).length > 0) {
          $('#record_table', this).append(tds);
        } else {
          $('#record_table').append(tds);
        }
      }
      $("#summary_result").text(calorie_summary);
      if (calorie_summary > 2000) {
        $('#summary_area').css('background-color', '#FF3333');
      };
      $("#input_date").val('');
      $("#input_time").val('');
      $("#input_meal").val('');
      $("#input_calorie").val('');
      $("#myModal").modal('hide');
    }
  });
};

function refreshTableWithResult(results) {
  for (var i = 0; i < results.length; i++) {
    var tds = '<tr>';
    var objId = results[i].objectId;
    var time = results[i].get('time');
    var options = {
      weekday: "long",
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit"
    };

    tds += '<td class="Time">' + results[i].get('time').toLocaleDateString("en-US", options) + '</td>';
    tds += '<td class="meal">' + results[i].get('meal') + '</td>';
    tds += '<td class="calorie">' + results[i].get('calorie') + '</td>';
    tds += '<td><button type="button" class="btn btn-success btn-sm edit_record">Edit</button></td>';
    tds += '<td><button type="button" class="btn btn-danger btn-sm delete_row">Delete</button></td>';
    tds += '<td hidden class="id">' + objId + '</td>';
    tds += '<td hidden class="time_date">' + results[i].get('time') + '</td>';
    tds += '</tr>';

    if ($('#record_table', this).length > 0) {
      $('#record_table', this).append(tds);
    } else {
      $('#record_table').append(tds);
    }
  }
  $("#input_date").val('');
  $("#input_time").val('');
  $("#input_meal").val('');
  $("#input_calorie").val('');
  $("#myModal").modal('hide');
}


/*
 $("#record_table").each(function () {
 var tds = '<tr>';
 tds += '<td class="date">' + date + '</td>';
 tds += '<td class="time">' + time + '</td>';
 tds += '<td class="meal">' + meal + '</td>';
 tds += '<td class="calorie">' + calorie + '</td>';
 tds+='<td><button type="button" class="btn btn-success btn-sm edit_record">Edit</button></td>';
 tds+='<td><button type="button" class="btn btn-danger btn-sm delete_row">Delete</button></td>';

 tds += '</tr>';
 if ($('tbody', this).length > 0) {
 $('tbody', this).append(tds);
 } else {
 $(this).append(tds);
 }
 $("#input_date").val('');
 $("#input_time").val('');
 $("#input_meal").val('');
 $("#input_calorie").val('');
 $("#myModal").modal('hide');
 }
 );*/

///User profile functions

function edit_profile() {
  var jun = moment("2014-06-01T12:00:00Z");

  $("#data_filter").click(function() {
    $("#data_modal").modal('show');
  });
  $("#load_record").click(function() {
    $("#data_modal").modal('hide');
  });
  //show record dialog

  $('#search').keypress(function (e) {

    if (e.which == 13) {
      var searchUrl = 'Http://localhost:8000/models/timezone/?search=' + $('#search').val();
      refreshTable(searchUrl);
    }
  });

  $("#create_record").click(function() {
    // var entry = {};
    // var max_id = -1;
    // for (var key in data) {
    //   if (parseInt(key) > max_id) max_id = parseInt(key);
    // }

    // var i = parseInt(max_id) + 1;
    // console.log(i);

    // data[i] = entry;

    // var tds = '<tr>';
    // var nameId = 'Name' + i.toString();
    // var timeId = 'Time' + i.toString();
    // var timezoneId = 'Timezone' + i.toString();

    // var tzNames = moment.tz.names();
    // var optionsAsString = '';
    // tzNames.forEach(function(tz) {
    //   optionsAsString += "<option value='" + tz + "'>" + tz + "</option>";
    // });

    // // tds += '<td hidden class="id">' + item['id'] + '</td>';
    // tds += '<td>' + '<a href="#" id="' + nameId + '" data-type="text" data-pk="1" data-title="Name of timezone">' + entry['name'] + '</a></td>';
    // tds += '<td id="' + timeId + '">' + "</td>";
    // tds += '<td>' + '<select class="form-control" id="' + timezoneId + '">' + optionsAsString + '</select></td>';
    // tds += '<td><button type="button" class="btn btn-danger btn-sm delete_row" id="Button' +i.toString() + '">Delete</button></td>';
    // tds += '</tr>';
    // updateId["#" + timeId] = "#" + timezoneId;

    var token = Cookies.get('toptal-token');
    entry = {};
    entry['name'] = 'New Clock';
    entry['timezone'] = 'America/Los_Angeles';
    $.ajax({
      url: 'http://localhost:8000/models/timezone/',
      method: 'POST',
      headers: {Authorization:'Token '+token},
      data: entry,
      success: function(res) {
        refreshTable('http://localhost:8000/models/timezone/');
      }
    });

    // if ($('#record_table', this).length > 0) {
    //   $('#record_table', this).append(tds);
    // } else {
    //   $('#record_table').append(tds);
    // }
    // $("#" + timezoneId).val(entry['timezone']);
    // $("#" + nameId).editable({
    //   success: function(response, newValue) {
    //     console.log($(this).parent().parent());
    //   }, error: function(response, newValue) {
    //     console.log('what');
    //   }
    // });
    // $("#myModal").modal('show');
    // $('#meal_time').val('Value');
    // var tzNames = moment.tz.names();
    // var optionsAsString = '';
    // tzNames.forEach(function(tz) {
    //   optionsAsString += "<option value='" + tz + "'>" + tz + "</option>";
    // });
    // $("select[id='timezone-select']").find('option').remove().end().append($(optionsAsString));
    // $("#timezone-select").change(function() {
    //   var selectedValue = $('#timezone-select').find(':selected').text();

    //   console.log(moment().tz(selectedValue).format());
    // });
  });
  //date and time picker
  $('.datepicker').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true
  });
  $('input.timepicker').timepicker({
    timeFormat: "H:i"
  });
  //edit record button
  $(document).on('click', 'button.edit_record', function() {
    $("#myModal").modal('show');
    var $row = $(this).closest("tr"); // Find the row
    var $objId = $row.find(".id").text();
    var $time = $row.find(".time_date").text();
    var $meal = $row.find(".meal").text();
    var $calorie = $row.find(".calorie").text();

    $("#input_meal").val($meal);
    $("#input_calorie").val($calorie);
  });
  //save record button
  $("#save_record").click(function() {
    var date = $("#meal_date").val();
    var time = $("#meal_time").val();
    var meal = $("#input_meal").val();

    var calorie = $("#input_calorie").val();
    var calorie_Data = Parse.Object.extend("calorie_Data");
    var calorie_data = new calorie_Data();
    var dt_str = date + " " + time;

    calorie_data.set("time", new Date(dt_str));
    calorie_data.set("meal", meal);
    calorie_data.set("calorie", calorie);
    calorie_data.set("user", Parse.User.current());
    calorie_data.save(null, {
      success: function(calorie_data) {
        refreshTable('http://localhost:8000/models/timezone/');
        // Find all posts by the current user
      }
    });
  });

  $(document).on('click', 'button.delete_row', function() {
    console.log($(this).context);
    var buttonId = parseInt($(this).context.id.replace("Button", ""));
    delete updateId["#" + buttonId];
    delete data[buttonId];

    var token = Cookies.get('toptal-token');
    $(this).closest('tr').remove();
    // Post delete data
    var url = 'http://localhost:8000/models/timezone/' + buttonId + '/';
    $.ajax({
      url: url,
      headers: {Authorization: 'Token ' + token},
      type: 'DELETE'
    });
  });
  refreshTable('http://localhost:8000/models/timezone/');
  $("#log_out").click(function() {
    Cookies.remove('token');
    location.href = "nice_signup.html";
  });
}
