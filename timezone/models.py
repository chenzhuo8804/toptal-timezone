# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
"""
Calorie Counter Models
"""

class Timezone(models.Model):
    name = models.CharField(max_length=100, blank=True, default='')
    timezone = models.CharField(max_length=100, blank=True, default='')
    user = models.ForeignKey('auth.User', related_name='timezone')

    def __str__(self):
        return self.name
