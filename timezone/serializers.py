from rest_framework import serializers
from .models import Timezone
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'is_staff')


class TimezoneSerializer(serializers.ModelSerializer):
    # user = UserSerializer()

    class Meta:
        model = Timezone
        fields = ('pk', 'name', 'timezone')
