"""
Views for Calorie Counter
"""
from django.db.models.query import Q
from rest_framework import viewsets
from rest_framework import filters
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, BasePermission, AllowAny
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from django.template.response import TemplateResponse

from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
import json

from .models import Timezone
from .serializers import TimezoneSerializer, UserSerializer


class IsStaffOrTargetUser(BasePermission):
    def has_permission(self, request, view):
        # allow user to list all users if logged in user is staff
        return view.action == 'retrieve' or request.user.is_staff
 
    def has_object_permission(self, request, view, obj):
        # allow logged in user to view own details, allows staff to view all records
        return request.user.is_staff or obj == request.user



class IsOwner(BasePermission):

    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        return obj.who == request.user


class UserViewSet(viewsets.ModelViewSet):
    model = User
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get_permission(self):
        return (AllowAny() if self.request.method == 'POST'
                else IsStaffOrTargetUser())

    def post_save(self, obj, created=False):
        if created:
            obj.set_password(obj.password)
            obj.save()


class TimezoneViewSet(viewsets.ModelViewSet):

    """
    Docstring for Timezone
    """
    queryset = Timezone.objects.all()
    serializer_class = TimezoneSerializer

    filter_backends = (filters.SearchFilter,)
    search_fields = ('timezone', 'name')

    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        if self.request.user.is_staff:
            return self.queryset
        else:
            return self.queryset.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)

    def delete(self, request, pk, format=None):
        timezone = self.get_object(pk)
        timezone.delete()


def index(request):
    response = TemplateResponse(request, 'index.html', {})
    return response
