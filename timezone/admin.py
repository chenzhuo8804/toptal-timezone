from django.contrib import admin
from .models import Timezone


class TimezoneAdmin(admin.ModelAdmin):
    pass

admin.site.register(Timezone, TimezoneAdmin)
